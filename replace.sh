# !/bin/bash

su -c cp -rfv ui/*.* /product/media/audio/ui/
su -c cp -rfv bootanimation.zip /product/media/
su -c cp -rfv logo.img /dev/block/bootdevice/by-name/logo
su -c cp -rfv NotoColorEmoji.ttf /system/fonts/

su -c chmod -R 644 /product/media/audio/ui/
su -c chmod 644 /product/media/bootanimation.zip
su -c chmod 777 /dev/block/bootdevice/by-name/logo
su -c chmod 644 /system/fonts/NotoColorEmoji.ttf

su -c rm /etc/hosts
su -c curl https://adaway.org/hosts.txt -o /etc/hosts
